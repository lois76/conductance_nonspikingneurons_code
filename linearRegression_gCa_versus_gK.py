# Linear regression for determining a quantitative relationship between the variations of gCa and gK

import numpy as np
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt

gCa = np.array([5.52, 5.32, 5.12, 4.92, 4.72, 4.52,  4.32, 4.12, 3.92, 3.72]).reshape((-1, 1))
gK = np.array([3, 2.7, 2.4, 2, 1.7, 1.35, 1, 0.7, 0.4, 0])
model = LinearRegression()
model.fit(gCa, gK)
r_sq = model.score(gCa, gK)

gCa_vec = np.arange(5.52, 3.7, -0.01) 
a = model.intercept_ 
b = model.coef_ 
print("a:", a)
print("b:", b)

def linearRegression(gCa):
	return a + b*gCa 

u = [4.92, 4.62, 4.32, 4.0, 3.72]
for g in u:
	print(linearRegression(g))
	
	
f = a + b*gCa_vec 

plt.plot(gCa_vec, f, 'r', label = "linear regression")
plt.scatter(gCa, gK, label = "interpolation points")
plt.legend()
plt.xlabel(r"$g_{Ca}$")
plt.ylabel(r"$g_K$")
plt.show()
